# xdots - dot files management #

This script allows to add / remove repos with dot files (or any other files) to your
home directory

### Usage ###
Install xdots
```bash
mkdir -p ~/dev
cd ~/dev
git clone https://aanatoly74@bitbucket.org/aanatoly74/xdots.git
export PATH="$HOME/bin:$HOME/dev/xdots:$PATH"
```

Add dots repo and test it
```bash
xdots -a -n main -u https://aanatoly74@bitbucket.org/aanatoly74/xdots-main.git
cd ~
xdots-main ls-files
xdots-main status
```

Then every dot repo is managed as a normal git repo via `xdots-name` wrapper

Add this line to `~/.bashrc` to have `xdots` and `xdots-*` in path
```
export PATH="$HOME/bin:$HOME/dev/xdots:$PATH"
```
